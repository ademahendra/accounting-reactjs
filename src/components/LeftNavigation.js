import React, { Component } from 'react';
// import AuthMiddleware from 'modules/auth/middleware';
import { Link } from 'react-router-dom';
import { uniqBy, sortBy } from 'lodash';

let menus = [{ key: 'j2', order: 2, title: 'Jurnal', path: '/jurnal', icon: 'pg-home', smalldesc: '12 New Updates', component: '' },
							{ key: 'as3', order: 3, title: 'Account', path: '/accountsetting', icon: 'pg-map', smalldesc: 'update account setting', component: '' },
							{ key: 'd4', order: 4, title: 'Department', path: '/department', icon: 'pg-layouts', smalldesc: 'update account setting', component: '' },
							{ key: 'h1', order: 1, title: 'Home', path: '/', icon: 'pg-home', component: '' },
							{ key: 'c1', order: 3, title: 'Customer', path: '/customer', icon: 'pg-home', component: '' }];

// just incase forget unique KEY
// we can remove this after the 'menus' fixed
uniqBy(menus, 'key');
// sort by order
menus = sortBy(menus, 'order');

const item = (x, i) => {
	return (
		<li className="navmenu" id={i} key={x.key}>
			<Link to={`${x.path}`} className="detailed" style={{ minHeight: '0px' }}>
				<span className="title">{x.title}</span>
				{ x.smalldesc ? <span className="details">{x.smalldesc}</span> : '' }
			</Link>
		</li>
	)
}

class LeftNavigation extends Component {
	render() {
		return (
			<nav className="page-sidebar" data-pages="sidebar" style={{ borderRight: '1px solid #e6e6e6' }}>
				<div className="sidebar-menu m-t-30">
					<ul className="menu-items">
						{menus.map((x, i) =>
								item(x, i)
							)}
					</ul>
					<div className="clearfix" />
				</div>
			</nav>
		);
	}
}

export default LeftNavigation;
