import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';

class AuthLogin extends Component {
  static propTypes = {
    isProcessing: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    signin: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      username: 'admin',
      password: 'qwerty',
      showPassword: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.togglePassword = this.togglePassword.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const credentials = {
      username: this.state.username,
      password: this.state.password,
      domain: this.state.domain
    };
    this.props.signin(credentials);
  }

  togglePassword() {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }

  render() {
    const { isProcessing, isError, errorMessage } = this.props;
    return (
      <div className="login-page-container">
        <div className="row">
          <div className="col-md-6">
            <div className="bg-caption p-l-20 m-b-20 ">
              <h2 className="semi-bold">
                Pages make it easy to enjoy what matters the most in the life
              </h2>
              <p className="small">
                images Displayed are solely for representation purposes only,
                All work copyright of respective owner, otherwise © 2013-2014
                REVOX.
              </p>
            </div>
          </div>
          <div className="col-sm-4 col-md-4">
            <div className="p-l-50 m-l-20 p-r-50 m-r-20 sm-p-l-15 sm-p-r-15">
              <div className="bold">Welcome</div>
              {isError ? <div>{errorMessage}</div> : null}
              <form className="p-t-15" onSubmit={this.onSubmit} name="form">
                <div className="form-group">
                  <input
                    value={this.state.username}
                    onChange={e => this.setState({ username: e.target.value })}
                    type="text"
                    className="form-control"
                    placeholder="Username"
                    required=""
                  />
                </div>
                <div className="form-group">
                  <input
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                    type={this.state.showPassword ? 'text' : 'password'}
                    placeholder="Password"
                    required=""
                    className="form-control"
                  />
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="checkbox">
                      <label htmlFor="remember">
                        <input id="remember" type="checkbox" />
                        <i className="info" /> Keep me signed in
                      </label>
                    </div>
                  </div>
                  <div className="col-md-6 text-right">
                    <button
                      type="submit"
                      className="btn btn-primary btn-cons m-r-0"
                      disabled={
                        !this.state.username ||
                        !this.state.password ||
                        isProcessing
                      }
                    >
                      {isProcessing ? 'Loading...' : 'Sign In'}
                    </button>
                  </div>
                </div>
              </form>
              <div className="m-y">
                <Link to="forget-password" className="_600">
                  Forgot password?
                </Link>
              </div>
              <div>
                Do not have an account?{' '}
                <Link to="signup" className="_600">
                  Sign up for free
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AuthLogin;
