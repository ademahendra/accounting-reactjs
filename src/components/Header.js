import React, { Component } from 'react';

class Header extends Component {
	render() {
		return (
			<div className="header" style={{ borderBottom: 'none' }}>
				<div className="container-fluid relative">
					<div className="pull-left full-height visible-sm visible-xs">
						<div className="header-inner">
							<button
								className="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5"
								data-toggle="sidebar"
							>
								<span className="icon-set menu-hambuger" />
							</button>
						</div>
					</div>
					<div className="pull-center hidden-md hidden-lg">
						<div className="header-inner">
							<div className="brand inline">
								<img
									alt="logo"
									data-src="assets/img/logo.png"
									data-src-retina="assets/img/logo_2x.png"
									height={22}
									src="assets/img/logo_2x.png"
									width={78}
								/>
							</div>
						</div>
					</div>
					<div className="pull-right full-height visible-sm visible-xs">
						<div className="header-inner">
							<a
								className="btn-link visible-sm-inline-block visible-xs-inline-block"
								data-toggle="quickview"
								data-toggle-element="#quickview"
								href="/landing"
							>
								<span className="icon-set menu-hambuger-plus" />
							</a>
						</div>
					</div>
				</div>
				<div className=" pull-right">
					<div className="visible-lg visible-md m-t-10">
						<div className="pull-left p-r-10 p-t-10 fs-16 font-heading">
							<span className="semi-bold">David</span>{' '}
							<span className="text-master">Nest</span>
						</div>
						<div className="dropdown pull-right">
							<button
								aria-expanded="false"
								aria-haspopup="true"
								className="profile-dropdown-toggle"
								data-toggle="dropdown"
							>
								<span className="thumbnail-wrapper d32 circular inline m-t-5">
									<img
										alt="avatar"
										data-src="assets/img/profiles/avatar.jpg"
										data-src-retina="assets/img/profiles/avatar_small2x.jpg"
										height={32}
										src="assets/img/profiles/avatar_small2x.jpg"
										width={32}
									/>
								</span>
							</button>
							<ul className="dropdown-menu profile-dropdown" role="menu">
								<li>
									<a href="/setting">
										<i className="pg-settings_small" /> Settings
									</a>
								</li>
								<li>
									<a href="/feedback">
										<i className="pg-outdent" /> Feedback
									</a>
								</li>
								<li>
									<a href="/help">
										<i className="pg-signals" /> Help
									</a>
								</li>
								<li className="bg-master-lighter">
									<a className="clearfix" href="/logout">
										<span className="pull-left">Logout</span>{' '}
										<span className="pull-right">
											<i className="pg-power" />
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Header;
