import LocalStorageManager from 'utils/LocalStorageManager';
import axios from 'axios';
import AuthActions from './actions';

export default class AuthMiddleware {

  // Signin Functions Starts
  static signin(credentials) {
    console.log('test ', credentials);
    return (dispatch) => {
      dispatch(AuthActions.signin());
      AuthMiddleware.signinWithUserCredentials(dispatch, credentials);
    };
  }

  static signinWithUserCredentials(dispatch, credentials) {
    axios({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      url: 'http://localhost:8080/v1/auth',
      responseType: 'json',
      data: {
        username: credentials.username,
        password: credentials.password
      }
    })
    .then((response) => {
      if (response && response.status === 200 && response.data) {
        // self.setState({data: response.data.items})
        if (credentials) {
            console.log('Sing IN successfull');
            LocalStorageManager.setUserToken(response.data.token);
            console.log(response.data.token);
            dispatch(AuthActions.signinSuccessful());
        } else {
          console.log('Sign IN Error');
          dispatch(AuthActions.signinRejected('error no credentials'));
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }
  // Signin Functions Ends

  // Logout Functions Starts
  static logout() {
    return (dispatch) => {
      dispatch(AuthActions.logout());
      AuthMiddleware.logoutFromAPI(dispatch);
    };
  }

  static logoutFromAPI(dispatch) {
    LocalStorageManager.removeUserToken();
    LocalStorageManager.clearLocalStorage();
    dispatch(AuthActions.logoutSuccessful());
  }
  // Logout Functions Ends

  // isLoggedIn
  static isLoggedIn() {
    return (dispatch) => {
      const token = LocalStorageManager.getUserToken();
      if (token) {
        AuthMiddleware.ensureAuthenticated(dispatch, token);
      } else {
        console.log('not logged in ');
        dispatch(AuthActions.isLoggedInFailure());
      }
    };
  }

  // ensureAuthenticated
  static ensureAuthenticated(dispatch, token) {
    if (token) {
      console.log('authentication successfull ');
      dispatch(AuthActions.isLoggedInSuccess());
    } else {
      // never gonna happen
      console.log('authentication error ');
      dispatch(AuthActions.signinRejected('no token'));
    }
  }
}
