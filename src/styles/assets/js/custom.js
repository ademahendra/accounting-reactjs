$('[data-pages="sidebar"]').each(function() {
    var $sidebar = $(this)
    $sidebar.sidebar($sidebar.data())
});

$('.dropdown-default').each(function() {
    var btn = $(this).find('.dropdown-menu').siblings('.dropdown-toggle');
    var offset = 0;

    var padding = btn.actual('innerWidth') - btn.actual('width');
    var menuWidth = $(this).find('.dropdown-menu').actual('outerWidth');

    if (btn.actual('outerWidth') < menuWidth) {
        btn.width(menuWidth - offset);
        $(this).find('.dropdown-menu').width(btn.actual('outerWidth'));
    } else {
        $(this).find('.dropdown-menu').width(btn.actual('outerWidth'));
    }
});