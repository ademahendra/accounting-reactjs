import React, { Component } from 'react';
import LoginContainer from 'containers/LoginContainer';
import '../../styles/base/login.css'

class Login extends Component {

  componentWillMount() {}
  componentWillUnmount() {}

  render() {
    return (
      <div>
        <LoginContainer />
      </div>
    );
  }
}

export default Login;
