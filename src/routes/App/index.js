import React from 'react';
import { Provider } from 'react-redux';
import {
	BrowserRouter,
	Route
} from 'react-router-dom';
import LayoutFull from './Layout';
import LoginLayout from './LoginLayout';

const supportsHistory = 'pushState' in window.history;

const layoutAssignments = {
	'/': LayoutFull,
	'/landing': LayoutFull,
	'/login': LoginLayout,
	'/accountsetting': LayoutFull
};

const layoutPicker = () => {
	console.log(location.pathname);
	const Layout = layoutAssignments[ location.pathname ];
	return Layout ? <Layout /> : <LayoutFull />;
};

const App = props => (
	<Provider store={props.store}>
		<BrowserRouter forceRefresh={!supportsHistory} keyLength={12}>
			<Route path="*" render={layoutPicker} />
		</BrowserRouter>
	</Provider>
);

App.propTypes = {
	store: React.PropTypes.object.isRequired
};

export default App;
