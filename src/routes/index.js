import asyncRoute from 'utils/asyncComponent';
import '../styles/base/bootstrap.css';
import '../styles/base/simple.scss';
// import '../styles/base/scss/pages.scss';
import '../styles/base/pages-icons.css';
// import '../styles/assets/plugins/jquery/jquery-1.11.1.min.js';
// import '../styles/assets/plugins/modernizr.custom.js';
// import '../styles/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js';
// import '../styles/assets/custom.js';

// Import causes routes to be code-split
// We have to specify each route name/path in order to be statically analyzed by webpack
export const Landing = asyncRoute(() => import('./Landing'));
export const Login = asyncRoute(() => import('./Login'));
export const Signup = asyncRoute(() => import('./Signup'));
export const Miss404 = asyncRoute(() => import('./Miss404'));
export const Home = asyncRoute(() => import('./Home'));
export const AccountSetting = asyncRoute(() => import('./AccountSetting'));

// Force import during development to enable Hot-Module Replacement
if (process.env.NODE_ENV === 'development') {
	require('./Landing'); // eslint-disable-line global-require
	require('./Login'); // eslint-disable-line global-require
	require('./Signup'); // eslint-disable-line global-require
	require('./Miss404'); // eslint-disable-line global-require
	require('./Home'); // eslint-disable-line global-require
	require('./AccountSetting'); // eslint-disable-line global-require
}
