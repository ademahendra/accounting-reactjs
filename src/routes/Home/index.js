import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  componentWillMount() {}
  componentWillUnmount() {}

  render() {
    return (
      <div>
        <div className="jumbotron" data-pages="parallax">
          <div className="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div className="inner">
              <ul className="breadcrumb">
                <li>
                  <p>Home Page</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="container-fluid container-fixed-lg">
          <Link to="/login">Go To Login</Link>
          <h5>landing is not allowed access until user logs in</h5>
        </div>
      </div>
    );
  }
}

export default Home;
